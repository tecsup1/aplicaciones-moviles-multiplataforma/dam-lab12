import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  // final options = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];
  final options = {
    'Alertas': Icon(Icons.add_alert, color: Colors.blue, size: 25.0),
    // 'Alertas':  FlutterLogo(),
    'Avatars': Icon(Icons.accessibility, color: Colors.blue, size: 25.0),
    'Cards - Tarjetas': Icon(Icons.folder_open, color: Colors.blue, size: 25.0),
    'Animated container': Icon(Icons.business_center, color: Colors.blue, size: 25.0),
    'Inputs': Icon(Icons.input, color: Colors.blue, size: 25.0),
    'Slider - Checks': Icon(Icons.playlist_add_check, color: Colors.blue, size: 25.0),
    'Listas y scroll': Icon(Icons.list, color: Colors.blue, size: 25.0),
  };
  TextStyle styleText = TextStyle(color: Colors.red, fontSize: 25.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Componentes'),
        ),
        // body: Align(
        //     child: Column(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: <Widget>[
        //         Text('Uno', style: styleText),
        //         Text('Dos', style: styleText),
        //         Text('Tres', style: styleText),
        //         Text('Cuatro', style: styleText),
        //       ],
        //     ),
        // ),
        // body: Center(
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       FlutterLogo(size:60.0,),
        //       FlutterLogo(size:60.0,),
        //       FlutterLogo(size:60.0,),
        //     ],
        // ),

        // punto 4.1 lab12:
        // body: Center(
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       FlutterLogo(size:60.0,),
        //       FlutterLogo(size:60.0,),
        //       FlutterLogo(size:60.0,),
        //     ],
        //   ),
        // );
        // body: Center(
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       Image.asset('assets/images/img1.png'),
        //       Icon(Icons.adb, color: Colors.red, size: 50.0),
        //       // Icon(Icons.beach_access, color: Colors.blue, size: 36.0),
        //       // Text("Prueba gonzalo2", style: styleText),
        //     ],
        //   ),
        // )

        // punto 7 del lab11:
        // body: ListView(
        //   children: <Widget>[
        //     ListTile(title: Text('Uno')),
        //     ListTile(title: Text('Dos')),
        //     ListTile(title: Text('Tres')),
        //   ],
        // )
        body: ListView(children: _crearItemsCortal()));
  }

  List<Widget> _crearItemsCortal() {
    // TODO: ya puedo acceder al valor y al key del diccionario, falta reemplazar en diccionario
    // el valor por los iconos para mostrarlos en la lista:
    return options.keys.map((item) {
      return Column(
        children: <Widget>[
          ListTile(
              leading: options[item],
              title: Text(item),
              subtitle: Text('Cualquier cosa'),
              trailing: Icon(Icons.keyboard_arrow_right)),
          Divider()
        ],
      );
    }).toList();
  }
}
